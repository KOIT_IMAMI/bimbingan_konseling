<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_login extends CI_Model
{
   
    public function login($u,$p)
    {
      $user = $this->db->where('USERNAME', $u)
                      ->where('PASSWORD', $p)
                      ->where('LEVEL', '2')
                      ->where('STATUS', 'AKTIF')
                      ->get('admin');
        

        if ($user->num_rows()>0) {
            foreach ($user->result() as $row) 
            {
                $sess = array('ID_ADMIN'    => $row->ID_ADMIN, 'is_login'=> true,'NAMA_ADMIN'=>$row->NAMA_ADMIN,'LEVEL'=> $row->LEVEL);
                $this->session->set_userdata($sess);
                return $user->num_rows();
            }
        }else{
            return 0;
        }
    }


    public function logout()
    {
        $data = [
            'username' => null,
            'level'    => null,
            'is_login' => null
        ];
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
    }
}
