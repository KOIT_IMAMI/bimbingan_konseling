<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_login extends CI_Controller
{
    public function index()
    {
    $this->load->view('v_login');        
    }

    public function getlogin()
    {
        $this->load->model('m_login');   
        $u = $this->input->post('username');
        $p = $this->input->post('password');
        $get_login=$this->m_login->login($u,$p);
        if ($get_login>0) {
            echo json_encode(array("status"=>true,'pesan'=>'valid'));
        }else{
            echo json_encode(array("status"=>true,'pesan'=>$get_login));
        }
    }

    public function logout()
    {
        $this->load->model('m_login'); 
        $this->m_login->logout();
        redirect(base_url('c_login'));
    }

    public function logout_ppnt()
    {
        $this->load->model('m_login'); 
        $this->m_login->logout();
        redirect(base_url('login'));
    }
}
