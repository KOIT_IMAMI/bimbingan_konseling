<!DOCTYPE html>
<?php
$cek_login = $this->session->userdata('is_login');
if (!empty($cek_login)) {
  redirect('C_dashboard');
}
?>
<?php 
$domain = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$domain .= "://".$_SERVER['HTTP_HOST'];
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LOGIN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/sweatalert/sweetalert.css">
<script src="<?php echo base_url() ?>assets/sweatalert/sweetalert-dev.js"></script>
<!-- iCheck -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url() ?>assets/index2.html"><b>BIMBINGAN LAYANAN BK</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">LOGIN APLIKASI</p>

    <form id="contact" onsubmit="cek_login()">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-lock"></i> LOGIN</button>
          <a type="submit" class="btn btn-primary btn-block btn-flat" href="<?php echo $domain ?>"><i class="glyphicon glyphicon-arrow-left"></i> KEMBALI HALAMAN UTAMA</a>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->

   

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->




<script type="text/javascript">
    function cek_login() {
      event.preventDefault();
      $.ajax({
          url : '<?php echo base_url() ?>C_login/getlogin',
          type : "POST",
          data : $('#contact').serialize(),
          dataType : 'json',
          success: function(data){
            if (data.pesan=='valid') {
             swal({
                title: "LOGIN SUKSES",
                text: "Anda Berhasil Login",
                type: "success",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
              },
              function(){
                setTimeout(function(){
                  window.location.href = '<?php echo base_url() ?>C_dashboard';
                }, 2000);
              });
            }else{
             swal("Login Gagal", "Username dan Password tidak valid..", "warning"); 
            }
            
          },
          error: function(jqXHR, textStatus, errorThrown){
            swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");

          }
        });
    }
   
</script>
</body>
</html>
