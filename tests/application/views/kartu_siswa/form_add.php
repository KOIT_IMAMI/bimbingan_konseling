<div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">DATA REKAP ABSENSI SISWA</h3>

              <div class="box-tools pull-right">
                <?php if ($status_form>0): ?>
                  <a href="" class="btn btn-danger" onclick="HapusData()"><i class="fa fa-trash"></i> HAPUS SEMUA DATA</a>
                <?php endif ?>
                
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="callout callout-warning">
                <h4>PERHATIAN (<?php echo $ket_form ?>)</h4>

                <p>Rekapan Absensi Siswa ini Hanya Berlaku untuk Layanan Bimbingan Konseling (BP/BK). <br> JENIS PEMBAYARAN UTAP :  <?php echo $KARTU_SISWA_JENIS ?> TAHUN AJARAN : <?php echo $TAHUN_AJARAN->TAHUN_AJARAN_NAMA ?></p>
              </div>
              <form id="form">
               <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>NO PESERTA</th>
                    <th>JENIS / THN</th>
                   
                  </tr>
                </thead>
                <tbody>

                    <input type="hidden" value="<?php echo $TAHUN_AJARAN->TAHUN_AJARAN_NAMA ?>" name="KARTU_SISWA_TAHUN_AJARAN">
                    <!-- <input type="hidden" value="<?php echo $KELAS_PARALEL ?>" id="KARTU_SISWA_KELAS" name="KARTU_SISWA_KELAS">
                    <input type="hidden" value="<?php echo $PARALEL ?>" id="KARTU_SISWA_PARALEL" name="KARTU_SISWA_PARALEL"> -->
                    <input type="hidden" value="<?php echo $KARTU_SISWA_JENIS ?>" id="KARTU_SISWA_JENIS" name="KARTU_SISWA_JENIS">
                    <input type="hidden" value="<?php echo $TAHUN_AJARAN_ID ?>" id="TAHUN_AJARAN_ID" name="TAHUN_AJARAN_ID">
                    
                    <input type="hidden" value="<?php echo $status_form ?>" name="STATUS_FORM">

                    <!-- UPDATE SISWA -->
                    <?php if ($status_form>0): ?>
                      <?php $no=1; foreach ($data_siswa as $row_data): ?>
                        <?php 
                          $value= $no%2==1?'#f9f174':'#fff';
                          $id = $row_data->ID_SISWA; 
                        ?>
                        <tr  style="background-color: <?php echo $value ?>">
                          <th><?php echo $no++ ?></th>
                          <th><?php echo $row_data->NO_INDUK_NISM ?></th>
                          <th><?php echo $row_data->NAMA_SISWA ?></th>
                          <th>
                            <input type="hidden" name="ID_SISWA<?php echo $id ?>" value="<?php echo $id ?>">
                            <input type="hidden" name="KARTU_SISWA_ID<?php echo $id ?>" value="<?php echo $row_data->KARTU_SISWA_ID ?>">
                            <input type="hidden" name="KARTU_SISWA_NAMA<?php echo $id ?>" value="<?php echo $row_data->NAMA_SISWA ?>">
                            <input type="hidden" name="KARTU_SISWA_NO_INDUK<?php echo $id ?>" value="<?php echo $row_data->NO_INDUK_NISM ?>">
                            <input type="radio" value="LUNAS" name="KARTU_SISWA_STATUS<?php echo $id ?>" class="flat-red" <?php echo $row_data->KARTU_SISWA_STATUS=='LUNAS'?'checked':'' ?>>LUNAS
                            <input type="radio" value="TIDAK LUNAS" name="KARTU_SISWA_STATUS<?php echo $id ?>" class="flat-red" <?php echo $row_data->KARTU_SISWA_STATUS=='TIDAK LUNAS'?'checked':'' ?>>TIDAK LUNAS
                            
                          </th>
                          <th>
                            <input type="text" name="KARTU_SISWA_KETERANGAN<?php echo $id ?>" class="form-control">
                          </th>
                        </tr>
                      <?php endforeach ?>
                    <?php endif ?>



                    <!-- input data siswa -->
                    <?php if ($status_form==0): ?>
                      <?php $no=1; foreach ($data_siswa as $row_data): ?>
                        <?php 
                          $value= $no%2==1?'#f9f174':'#fff';
                          $id = $row_data->ID_SISWA;
                          if ($no<10) {
                            $no_peserta = "00".$no;
                          }else if ($no<100) {
                            $no_peserta = "0".$no;
                          }else{
                            $no_peserta = $no;
                          }

                          $no_kartu = "31-512-".$no_peserta."-".$row_data->KELAS_PARALEL; 
                        ?>
                        <tr  style="background-color: <?php echo $value ?>">
                          <th><?php echo $no++ ?></th>
                          <th><?php echo $row_data->NO_INDUK_NISM ?></th>
                          <th><?php echo $row_data->NAMA_SISWA ?></th>
                          <th>
                            <input type="hidden" name="ID_SISWA<?php echo $id ?>" value="<?php echo $id ?>">
                            <input type="hidden" name="KARTU_SISWA_NAMA<?php echo $id ?>" value="<?php echo $row_data->NAMA_SISWA ?>">
                            <input type="hidden" name="KARTU_SISWA_NO_INDUK<?php echo $id ?>" value="<?php echo $row_data->NO_INDUK_NISM ?>">
                            <input type="text" value="<?php echo $no_kartu ?>" name="KARTU_SISWA_RUANGAN<?php echo $id ?>" class="form-control" readonly>
                          </th>
                         <th><?php echo $row_data->KELAS_PARALEL." ( ".$row_data->PARALEL.")"; ?></th>
                        </tr>
                      <?php endforeach ?>

                    <?php endif ?>

                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="7">
                      <input type="hidden" name="jumlah_row" value="<?php echo $no-1 ?>">
                       <button class="btn btn-primary" id="btn_save" onclick="btn_simpan()"><i class="fa fa-save"></i> SIMPAN DATA</button>
              
                     
                    </td>
                  </tr>
                </tfoot>
                
              </table>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
    })
  })
</script>
<script type="text/javascript">
  $(function () {
   //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })
</script>
<script type="text/javascript">
  function btn_simpan() {
    event.preventDefault();
    $('#btn_save').attr('disabled',true);
    $('#progress').load('<?php echo base_url() ?>C_utap_siswa/progress/0');
    $.ajax({
      url : '<?php echo base_url() ?>C_utap_siswa/simpan_data',
      type : "POST",
      data : $('#form').serialize(),
      dataType : 'json',
      success: function(){
        $('#progress').load('<?php echo base_url() ?>C_utap_siswa/progress/1');
        setTimeout(function() {
          $('#progress').load('<?php echo base_url() ?>C_utap_siswa/progress/2');
        },700)
      },
      error: function(jqXHR, textStatus, errorThrown){
        swal("ERORR", "TERJADI KESALAHAN SISTEM", "error");
      }
    });
  }
</script>

<script type="text/javascript">
   function HapusData(){
    event.preventDefault();
      swal({
        title: "HAPUS DATA ABSENSI",
        text: "Data Dalam Filter Ini dihapus Semua??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Hapus!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(){
         $.ajax({
            url : '<?php echo base_url() ?>C_absensi_siswa/hapus_data',
            type : "POST",
            data : $('#form').serialize(),
            dataType : 'json',
            success: function(data)
            {
              setTimeout(function() {
                 swal({
                  title: "DATA DIHAPUS!!",
                  text: "Data Berhasil Di Hapus",
                  type: "warning",
                  showCancelButton: false,
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true,
                },
                function(){
                  setTimeout(function(){
                    location.reload();
                  }, 500);
                });
              },500);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
        
      });
    };
  </script>