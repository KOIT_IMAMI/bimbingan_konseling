<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">MTsS NURUT TAQWA</span>
              <span class="info-box-number">DATA ABSENSI HARIAN SISWA</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    DATA ABSENSI HARIAN SISWA
                  </span>
            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div id="progress"></div>

       
        </div>

        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">FILTER DATA ABSENSI SISWA</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="col-sm-6">
                <!-- <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">KELAS SISWA</label>
                  <div class="col-sm-4">
                   <select id="KELAS_PARALEL" class="form-control select2">
                     <option value="7">7</option>
                     <option value="9">8</option>
                     <option value="9">9</option>
                   </select>
                  </div>
                  <div class="col-sm-4">
                   <select id="PARALEL" class="form-control select2">
                     <option value="1">A</option>
                     <option value="2">B</option>
                     <option value="3">C</option>
                     <option value="4">D</option>
                   </select>
                  </div>
                </div> -->
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">TAHUN AJARAN</label>
                  <div class="col-sm-5">
                    <select id="TAHUN_AJARAN" class="form-control select2">
                     <option value="1">TAHUN AJARAN</option>
                     <?php foreach ($data_tahun_ajaran as $row_data): ?>
                       <option value="<?php echo $row_data->TAHUN_AJARAN_ID ?>"><?php echo $row_data->TAHUN_AJARAN_NAMA ?></option>
                     <?php endforeach ?>
                   </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">JENIS UTAP</label>
                  <div class="col-sm-5">
                    <select id="KARTU_SISWA_JENIS" class="form-control select2">
                     <option value="">JENIS UTAP</option>
                     <option value="PTS GANJIL">PTS GANJIL</option>
                     <option value="PTS GENAP">PTS GENAP</option>
                     <option value="PAS GANJIL">PAS GANJIL</option>
                     <option value="PAS GENAP">PAS GENAP</option>
                    
                   </select>
                  </div>
                </div>
              </div>
              
               
              </div>

              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-info pull-right" onclick="btn_cari()"><i class="fa fa-search"></i> CARI DATA SISWA</button>
              </div>
              
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
        <div id="tampil_data"></div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
    function btn_cari() {
      var TAHUN_AJARAN = $('#TAHUN_AJARAN').val();
      var KARTU_SISWA_JENIS = $('#KARTU_SISWA_JENIS').val();
      if (TAHUN_AJARAN==""||KARTU_SISWA_JENIS=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#tampil_data').load('<?php echo base_url() ?>C_kartu_siswa/form_add',{
          'TAHUN_AJARAN':TAHUN_AJARAN,
          'KARTU_SISWA_JENIS':KARTU_SISWA_JENIS
        });
      }
      
      
    }

    // function btn_cari() {
    //   var KELAS_PARALEL = $('#KELAS_PARALEL').val();
    //   var PARALEL = $('#PARALEL').val();
    //   var TAHUN_AJARAN = $('#TAHUN_AJARAN').val();
    //   var KARTU_SISWA_JENIS = $('#KARTU_SISWA_JENIS').val();
    //   if (KELAS_PARALEL==""||PARALEL==""||TAHUN_AJARAN==""||KARTU_SISWA_JENIS=="") {
    //     swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
    //   }else{
    //     $('#tampil_data').load('<?php echo base_url() ?>C_utap_siswa/form_add',{
    //       'KELAS_PARALEL':KELAS_PARALEL,
    //       'PARALEL':PARALEL,
    //       'TAHUN_AJARAN':TAHUN_AJARAN,
    //       'KARTU_SISWA_JENIS':KARTU_SISWA_JENIS
    //     });
    //   }
      
      
    // }
  </script>