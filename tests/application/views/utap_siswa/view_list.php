<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DATA REKAP ABSENSI SISWA
        <small>MTsS NURUT TAQWA</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">DATA PRESENSI SISWA</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">MTsS NURUT TAQWA</span>
              <span class="info-box-number">DATA ABSENSI HARIAN SISWA</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    DATA ABSENSI HARIAN SISWA
                  </span>
            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
       
        </div>
        <div class="col-md-12">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">FILTER DATA REKAP PRESENSI</h3>

              <div class="box-tools pull-right">
               
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-body">
               <form class="form-horizontal" id="form" action="<?php echo base_url() ?>C_absensi_siswa/cetak" method="POST" target="_BLANK">
              <div class="box-body">
                <div class="col-sm-8">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">KELAS SISWA</label>
                  <div class="col-sm-4">
                   <select id="UTAP_PESANTREN_KELAS" name="UTAP_PESANTREN_KELAS" class="form-control select2" onchange="ambil_data_siswa()">
                     <option value="">Pilih Kelas</option>
                     
                     <option value="7">7</option>
                     <option value="9">8</option>
                     <option value="9">9</option>
                   </select>
                  </div>
                  <div class="col-sm-4">
                   <select id="UTAP_PESANTREN_PARALEL" name="UTAP_PESANTREN_PARALEL" class="form-control select2" onchange="ambil_data_siswa()">
                    <option value="">Pilih Paralel</option>
                   
                     <option value="1">A</option>
                     <option value="2">B</option>
                     <option value="3">C</option>
                     <option value="4">D</option>
                   </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">TAHUN AJARAN</label>
                  <div class="col-sm-4">
                    <select id="UTAP_PESANTREN_TAHUN_AJARAN" name="UTAP_PESANTREN_TAHUN_AJARAN" class="form-control select2">
                     <option value="1">TAHUN AJARAN</option>
                     <?php foreach ($data_tahun_ajaran as $row_data): ?>
                       <option value="<?php echo $row_data->TAHUN_AJARAN_ID ?>"><?php echo $row_data->TAHUN_AJARAN_NAMA ?></option>
                     <?php endforeach ?>
                   </select>
                  </div>
                   <div class="col-sm-4">
                    <select id="UTAP_PESANTREN_JENIS" name="UTAP_PESANTREN_JENIS" class="form-control select2">
                     <option value="">JENIS UTAP</option>
                     <option value="PTS GANJIL">PTS GANJIL</option>
                     <option value="PTS GENAP">PTS GENAP</option>
                     <option value="PAS GANJIL">PAS GANJIL</option>
                     <option value="PAS GENAP">PAS GENAP</option>
                     
                   </select>
                  </div>
                </div>
               
                <div id="nama_siswa">
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">NAMA SISWA (OPSIONAL)</label>
                      <div class="col-sm-8">
                       <select id="ID_SISWA" name="ID_SISWA" class="form-control select2" >
                         <option value="">Semua Siswa</option>
                       </select>
                      </div>
                    </div>
                </div>
               

                
                
              </div>
              
              
              </div>

              <!-- /.box-body -->
              <div class="box-footer pull-right" >
               <!--  <a onclick="cetak_rekap_data()" target="_BLANK" class="btn btn-success"><i class="fa fa-print"></i> CETAK REKAP DATA</a>
                <a onclick="cetak_data()" target="_BLANK" class="btn btn-success"><i class="fa fa-print"></i> CETAK DATA</a> -->
                <a onclick="btn_cari()" target="_BLANK" class="btn btn-warning"><i class="fa fa-search"></i> CARI DATA SISWA</a>
              </div>
              
              <!-- /.box-footer -->
            </form>
            </div>

          </div>
          
        </div>
        <div id="tampil_data">
          
        </div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
    $('#tampil_data').load('<?php echo base_url() ?>C_utap_siswa/list_data');
    function btn_cari() {
      var UTAP_PESANTREN_KELAS = $('#UTAP_PESANTREN_KELAS').val();
      var UTAP_PESANTREN_PARALEL = $('#UTAP_PESANTREN_PARALEL').val();
      
      var UTAP_PESANTREN_TAHUN_AJARAN = $('#UTAP_PESANTREN_TAHUN_AJARAN').val();
      var UTAP_PESANTREN_JENIS = $('#UTAP_PESANTREN_JENIS').val();
      var ID_SISWA = $('#ID_SISWA').val();
      if (UTAP_PESANTREN_KELAS==""||UTAP_PESANTREN_PARALEL==""||UTAP_PESANTREN_TAHUN_AJARAN==""||UTAP_PESANTREN_JENIS=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#tampil_data').load('<?php echo base_url() ?>C_utap_siswa/data',{
          'UTAP_PESANTREN_KELAS':UTAP_PESANTREN_KELAS,
          'UTAP_PESANTREN_PARALEL':UTAP_PESANTREN_PARALEL,
          'UTAP_PESANTREN_TAHUN_AJARAN':UTAP_PESANTREN_TAHUN_AJARAN,
          'UTAP_PESANTREN_JENIS':UTAP_PESANTREN_JENIS,
          'ID_SISWA':ID_SISWA
        });
      }
      
      
    }

    function ambil_data_siswa(){
      var UTAP_PESANTREN_KELAS = $('#UTAP_PESANTREN_KELAS').val();
      var UTAP_PESANTREN_PARALEL = $('#UTAP_PESANTREN_PARALEL').val();
      
      if(UTAP_PESANTREN_PARALEL==""||UTAP_PESANTREN_PARALEL==""){
        $('#ID_SISWA').attr('disabled',true);
      }else{
        $('#ID_SISWA').attr('disabled',false);
        $('#nama_siswa').load('<?php echo base_url() ?>C_utap_siswa/ambil_data_siswa',{
          'UTAP_PESANTREN_KELAS':KELAS_PARALEL,
          'UTAP_PESANTREN_PARALEL':PARALEL
        });
      }
      
    }

    function cetak_data(){
      var KELAS_PARALEL = $('#KELAS_PARALEL').val();
      var PARALEL = $('#PARALEL').val();
      
      var TAHUN_AJARAN = $('#TAHUN_AJARAN').val();
      var SEMESTER = $('#SEMESTER').val();
      var TANGGAL_MULAI = $('#TANGGAL_MULAI').val();
      var TANGGAL_AKHIR = $('#TANGGAL_AKHIR').val();
      var ID_SISWA = $('#ID_SISWA').val();
      if (KELAS_PARALEL==""||PARALEL==""||TAHUN_AJARAN==""||SEMESTER==""||TANGGAL_MULAI==""||TANGGAL_AKHIR=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#form').attr('action','<?php echo base_url() ?>C_absensi_siswa/cetak');
        $('#form').submit();
      }
      
    }

    function cetak_rekap_data(){
      var KELAS_PARALEL = $('#KELAS_PARALEL').val();
      var PARALEL = $('#PARALEL').val();
      
      var TAHUN_AJARAN = $('#TAHUN_AJARAN').val();
      var SEMESTER = $('#SEMESTER').val();
      var TANGGAL_MULAI = $('#TANGGAL_MULAI').val();
      var TANGGAL_AKHIR = $('#TANGGAL_AKHIR').val();
      var ID_SISWA = $('#ID_SISWA').val();
      if (KELAS_PARALEL==""||PARALEL==""||TAHUN_AJARAN==""||SEMESTER==""||TANGGAL_MULAI==""||TANGGAL_AKHIR=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#form').attr('action','<?php echo base_url() ?>C_absensi_siswa/cetak_rekap_data');
        $('#form').submit();
      }
      
    }
  </script>