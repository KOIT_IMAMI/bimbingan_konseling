<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	<table border="0">
		<tr>
			<td style="width: 10%;border-bottom: 1px solid #000;">
				<img src="<?php echo base_url() ?>assets/logomts.png">
			</td>
			<td style="border-bottom: 1px solid #000;">
				<span>YAYYAYASAN NURUT TAQWA</span> <br>
				<span>MADRASAH TSANAWIYAH NURUT TAQWA</span> <br>
				<span>GRUJUGAN CERMEE BONDOWOSO 68286</span> <br>
				<span>Jl. Raya Cermee No. 09 Grujugan Cermee Bondowoso 0332-561605</span> <br>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<span style="font-weight: bold;font-family: times;font-size: 14px;">REKAP ABSENSI SISWA (Bimbingan Konseling)</span>
			</td>
		</tr>
		
	</table>

               <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>TANGGAL</th>
                    <th>JAM I</th>
                    <th>JAM II</th>
                    <th>JAM III</th>
                    <th>JAM IV</th>
                  </tr>
                </thead>

                <tbody style="font-size: 12px">
                  <?php $no=1; foreach ($r_data as $row_data): ?>
                  <?php
                    $H =   '';
                    $A = '<i class="glyphicon glyphicon-off danger"></i>';
                  ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row_data->SISWA_NO_INDUK ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_NAMA ?></td>
                      <td><?php echo $this->M_public_function->get_date($row_data->ABSENSI_SISWA_TGL) ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_I=='H' ?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_II=='H'?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_III=='H'?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_IV=='H'?$H:$A ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
               
              </table>

</body>
</html>
<script type="text/javascript">
	window.print();
</script>