<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_dashboard extends CI_Controller {

	public function index()
	{
		$filter = array(
			'RUANG_KELAS_STATUS'=>'1'
		);
		$data['data_ruang_kelas'] = $this->M_apps->check_data_result($filter,'ruang_kelas');
		$data['KELAS_VII'] = $this->db->query("SELECT COUNT(ID_SISWA) as JML_SISWA FROM tb_siswa_lengkap WHERE KELAS_PARALEL ='7'")->row();
		$data['KELAS_VIII'] = $this->db->query("SELECT COUNT(ID_SISWA) as JML_SISWA FROM tb_siswa_lengkap WHERE KELAS_PARALEL ='8'")->row();
		$data['KELAS_IX'] = $this->db->query("SELECT COUNT(ID_SISWA) as JML_SISWA FROM tb_siswa_lengkap WHERE KELAS_PARALEL ='9'")->row();
		$data['KELAS_AKTIF'] = $this->db->query("SELECT COUNT(ID_SISWA) as JML_SISWA FROM tb_siswa_lengkap WHERE STATUS_KEAKTIFAN ='AKTIF'")->row();
		$data['konten'] = 'dashboard_absensi';
		$this->load->view('tampilan_backend',$data);
	}

	
}
