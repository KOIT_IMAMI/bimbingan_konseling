<!DOCTYPE html>
<html>
<head>
  <title></title>
   <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
  <table border="0">
    <tr>
      <td style="width: 10%;border-bottom: 1px solid #000;">
        <img src="<?php echo base_url() ?>assets/logomts.png">
      </td>
      <td style="border-bottom: 1px solid #000;">
        <span style="font-weight: bold;font-size: 18px;font-family: arial">YAYASAN NURUT TAQWA</span> <br>
        <span style="font-weight: bold;font-size: 16px;font-family: arial">MADRASAH TSANAWIYAH NURUT TAQWA</span> <br>
        <span style="font-weight: bold;font-size: 12px;font-family: arial">GRUJUGAN CERMEE BONDOWOSO 68286</span> <br>
        <span style="font-weight: bold;font-size: 10px;font-family: arial">Jl. Raya Cermee No. 09 Grujugan Cermee Bondowoso 0332-561605</span> <br>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <span style="font-weight: bold;font-family: times;font-size: 14px;">REKAP ABSENSI SISWA (Bimbingan Konseling)</span>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        PERIODE : <?php echo $this->M_public_function->get_date($TANGGAL_MULAI)." Sd. ".$this->M_public_function->get_date($TANGGAL_AKHIR)  ?>
      </td>
    </tr>
    
  </table>

               <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>JAM I</th>
                    <th>JAM II</th>
                    <th>JAM III</th>
                    <th>JAM IV</th>
                    <th>JUMLAH</th>
                  </tr>
                </thead>

                <tbody style="font-size: 12px">
                  <?php $no=1; foreach ($r_data as $row_data): ?>
                  <?php
                    $H =   '';
                    $A = '<i class="glyphicon glyphicon-off danger"></i>';
                  ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row_data->SISWA_NO_INDUK ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_NAMA ?></td>
                      
                      <td><?php echo $row_data->JAM1 ?></td>
                      <td><?php echo $row_data->JAM2 ?></td>
                      <td><?php echo $row_data->JAM3 ?></td>
                      <td><?php echo $row_data->JAM4 ?></td>
                      <td>
                        <?php echo $row_data->JAM4+$row_data->JAM3+$row_data->JAM2+$row_data->JAM1 ?>
                          
                        </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
               
              </table>

</body>
</html>
<script type="text/javascript">
  window.print();
</script>