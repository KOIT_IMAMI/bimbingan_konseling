 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        HALAMAN DASHBOARD
        <small>HALAMAN DASHBOARD</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">HALAMAN DASHBOARD</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

     
      <!-- =========================================================== -->

      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SISWA AKTIF</span>
              <span class="info-box-number"><?php echo $KELAS_AKTIF->JML_SISWA ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> RUANGAN KELAS</span>
              <span class="info-box-number">9 Rombel</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL ABSENSI</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      
      </div>
      <!-- /.row -->
        <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $KELAS_IX->JML_SISWA ?></h3>

              <p>KELAS IX</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $KELAS_VIII->JML_SISWA ?></h3>

              <p>KELAS VIII</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $KELAS_VII->JML_SISWA ?></h3>

              <p>KELAS VII</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>JADWAL PELAJARAN</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- =========================================================== -->

   

      <div class="row">
        
        <!-- /.col -->
        <div class="col-md-12">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <!-- <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image"> -->
                <span class="username"><a href="#">MTs NURUT TAQWA</a></span>
                <span class="description">LAPORAN ABSENSI SISWA </span>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
               
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
               
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                   <div class="col-sm-4">
                    <input type="date" name="" id="tanggal_mulai" class="form-control">
                  </div>
                  <div class="col-sm-4">
                    <input type="date" name="" id="tanggal_akhir" class="form-control">
                  </div>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-info pull-right" onclick="filter_grafik()"><i class="fa fa-search"></i> LIST DATA</button>
                  </div>
                </div>
              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                
              </div>
              <!-- /.box-footer -->
            </form>
            <div id="grafik">
              <div id="container" style="height: 100%"></div>
            </div>
              
            </div>
            <!-- /.box-body -->
            
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK DATA REKAP ABSENSI SISWA MTs NURUT TAQWA'
    },
    subtitle: {
        text: 'DATA REKAP ABSENSI SISWA'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Alpha....'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f} A'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} JAM</b> of total<br/>'
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        <?php foreach ($data_ruang_kelas as $r_data): ?>
          <?php $kelas = $r_data->RUANG_KELAS_NAMA; $paralel = $r_data->RUANG_KELAS_PARALEL; $jml_absensi_perkelas = $this->db->query("SELECT SUM(ABSENSI_SISWA_JAM_I+ABSENSI_SISWA_JAM_II+ABSENSI_SISWA_JAM_III+ABSENSI_SISWA_JAM_IV) AS JML, ABSENSI_SISWA_PARALEL, SISWA_KELAS from absensi_siswa WHERE SISWA_KELAS = '$kelas' AND ABSENSI_SISWA_PARALEL ='$paralel'")->row(); if(empty($jml_absensi_perkelas->JML)){$jml_absensi_perkelas->JML=0;}?>
         {
            name: '<?php echo $r_data->RUANG_KELAS_SEBUTAN ?>',
            y: <?php echo $jml_absensi_perkelas->JML ?>,
            drilldown: '<?php echo "KELAS_".$kelas."-".$paralel ?>'
        },
        <?php endforeach ?>
        ]
    }],
    drilldown: {
        series: [
        <?php foreach ($data_ruang_kelas as $r_kelas): ?>
          <?php $kelas = $r_kelas->RUANG_KELAS_NAMA; $paralel = $r_kelas->RUANG_KELAS_PARALEL; $siswa_kelas = $this->db->query("SELECT *,SUM(ABSENSI_SISWA_JAM_I+ABSENSI_SISWA_JAM_II+ABSENSI_SISWA_JAM_III+ABSENSI_SISWA_JAM_IV) AS JML from absensi_siswa WHERE SISWA_KELAS = '$kelas' AND ABSENSI_SISWA_PARALEL ='$paralel' group by ID_SISWA")->result(); ?>
          {
            name: '<?php echo "KELAS_".$r_kelas->RUANG_KELAS_NAMA."-".$r_kelas->RUANG_KELAS_PARALEL ?>',
            id: '<?php echo "KELAS_".$r_kelas->RUANG_KELAS_NAMA."-".$r_kelas->RUANG_KELAS_PARALEL ?>',
            data: [
              <?php foreach ($siswa_kelas as $data_kelas): ?>
                <?php $nama_siswa = str_replace(" ", "_", $data_kelas->ABSENSI_SISWA_NAMA); ?>
                [
                    '<?php echo $nama_siswa ?>',
                    <?php echo $data_kelas->JML ?>
                ],
              <?php endforeach ?>
                
            ]
        },
        <?php endforeach ?>
        ]
    }
});
    </script>
    <script type="text/javascript">
      function filter_grafik() {
        // body...
        event.preventDefault();
        var tanggal_akhir = $('#tanggal_akhir').val();
        var tanggal_mulai = $('#tanggal_mulai').val();
        if (tanggal_akhir==""||tanggal_mulai=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#grafik').load('<?php echo base_url() ?>C_dashboard/filter_grafik',{
          'tanggal_akhir':tanggal_akhir,
          'tanggal_mulai':tanggal_mulai
        });
      }  
      }
    </script>