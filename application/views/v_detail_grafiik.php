<div id="container" style="height: 100%"></div>
<script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'GRAFIK DATA REKAP ABSENSI SISWA MTs NURUT TAQWA'
    },
    subtitle: {
        text: 'DATA REKAP ABSENSI SISWA'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Alpha....'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f} A'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} JAM</b> of total<br/>'
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        <?php foreach ($data_ruang_kelas as $r_data): ?>
          <?php $kelas = $r_data->RUANG_KELAS_NAMA; $paralel = $r_data->RUANG_KELAS_PARALEL; $jml_absensi_perkelas = $this->db->query("SELECT SUM(ABSENSI_SISWA_JAM_I+ABSENSI_SISWA_JAM_II+ABSENSI_SISWA_JAM_III+ABSENSI_SISWA_JAM_IV) AS JML, ABSENSI_SISWA_PARALEL, SISWA_KELAS from absensi_siswa WHERE SISWA_KELAS = '$kelas' AND ABSENSI_SISWA_PARALEL ='$paralel' AND ABSENSI_SISWA_TGL BETWEEN '$tanggal_mulai' AND '$tanggal_akhir'")->row(); if(empty($jml_absensi_perkelas->JML)){$jml_absensi_perkelas->JML=0;}?>
         {
            name: '<?php echo $r_data->RUANG_KELAS_SEBUTAN ?>',
            y: <?php echo $jml_absensi_perkelas->JML ?>,
            drilldown: '<?php echo "KELAS_".$kelas."-".$paralel ?>'
        },
        <?php endforeach ?>
        ]
    }],
    drilldown: {
        series: [
        <?php foreach ($data_ruang_kelas as $r_kelas): ?>
          <?php $kelas = $r_kelas->RUANG_KELAS_NAMA; $paralel = $r_kelas->RUANG_KELAS_PARALEL; $siswa_kelas = $this->db->query("SELECT *,SUM(ABSENSI_SISWA_JAM_I+ABSENSI_SISWA_JAM_II+ABSENSI_SISWA_JAM_III+ABSENSI_SISWA_JAM_IV) AS JML from absensi_siswa WHERE SISWA_KELAS = '7' AND ABSENSI_SISWA_PARALEL ='1' AND ABSENSI_SISWA_TGL BETWEEN '$tanggal_mulai' AND '$tanggal_akhir' group by ID_SISWA")->result(); ?>
          {
            name: '<?php echo "KELAS_".$r_kelas->RUANG_KELAS_NAMA."-".$r_kelas->RUANG_KELAS_PARALEL ?>',
            id: '<?php echo "KELAS_".$r_kelas->RUANG_KELAS_NAMA."-".$r_kelas->RUANG_KELAS_PARALEL ?>',
            data: [
              <?php foreach ($siswa_kelas as $data_kelas): ?>
                <?php $nama_siswa = str_replace(" ", "_", $data_kelas->ABSENSI_SISWA_NAMA); ?>
                [
                    '<?php echo $nama_siswa ?>',
                    <?php echo $data_kelas->JML ?>
                ],
              <?php endforeach ?>
                
            ]
        },
        <?php endforeach ?>
        ]
    }
});
    </script>