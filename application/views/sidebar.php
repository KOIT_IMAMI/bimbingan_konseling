<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/logo.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>MTsS NURUT TAQWA</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>C_dashboard"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
           
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>PRESENSI SISWA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>C_absensi_siswa"><i class="fa fa-circle-o"></i> PRESENSI SISWA</a></li>
            <li><a href="<?php echo base_url() ?>C_absensi_siswa/form"><i class="fa fa-circle-o"></i> TAMBAH DATA</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>UTAP SISWA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>C_utap_siswa"><i class="fa fa-circle-o"></i> UTAP SISWA</a></li>
            <li><a href="<?php echo base_url() ?>C_utap_siswa/form"><i class="fa fa-circle-o"></i> TAMBAH DATA UTAP</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>KARTU PESERTA SISWA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() ?>C_kartu_siswa"><i class="fa fa-circle-o"></i> KARTU PESERTA SISWA</a></li>
            <li><a href="<?php echo base_url() ?>C_kartu_siswa/form"><i class="fa fa-circle-o"></i> TAMBAH KARTU PESERTA SISWA</a></li>
            
          </ul>
        </li>
       
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>