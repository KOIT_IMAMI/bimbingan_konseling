<?php if ($status==2): ?>
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-warning"></i> PEMBERITAHUAN PENYIMPANAN</h4>
  
  <div class="progress progress active">
      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        <span class="sr-only">20% Complete</span>
      </div>
   </div>
    Proses Penyimpanan Data Rekap Siswa Berhasil Disimpan!!!, Silahkan Refresh Halaman Atau Tekan Tombol F5
</div>
<?php endif ?>
<?php if ($status==1): ?>
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-warning"></i> PEMBERITAHUAN PENYIMPANAN</h4>
  
  <div class="progress progress active">
      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
        <span class="sr-only">20% Complete</span>
      </div>
   </div>
   Proses Sedang Berlangsung, Jangan Mencoba Menutup Aplikasi Ini Sebelum Proses ini selesai!!!!
</div>
<?php endif ?>
<?php if ($status==0): ?>
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-warning"></i> PEMBERITAHUAN PENYIMPANAN</h4>
  
  <div class="progress progress active">
      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 35%">
        <span class="sr-only">20% Complete</span>
      </div>
   </div>
   Proses Sedang Berlangsung, Jangan Mencoba Menutup Aplikasi Ini Sebelum Proses ini selesai!!!!
</div>
<?php endif ?>