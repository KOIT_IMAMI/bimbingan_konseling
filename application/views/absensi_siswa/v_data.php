<div class="col-md-12">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">DATA REKAP ABSENSI SISWA</h3>

              <div class="box-tools pull-right">
               
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">              
               <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>TANGGAL</th>
                    <th>JAM I</th>
                    <th>JAM II</th>
                    <th>JAM III</th>
                    <th>JAM IV</th>
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1; foreach ($r_data as $row_data): ?>
                  <?php
                    $H =   '<button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>';
                    $A = '<button class="btn btn-danger"><i class="glyphicon glyphicon-off"></i></button>';
                  ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row_data->SISWA_NO_INDUK ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_NAMA ?></td>
                      <td><?php echo $this->M_public_function->get_date($row_data->ABSENSI_SISWA_TGL) ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_I=='0'?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_II=='0'?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_III=='0'?$H:$A ?></td>
                      <td><?php echo $row_data->ABSENSI_SISWA_JAM_IV=='0'?$H:$A ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
               
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
    })
  })
</script>
