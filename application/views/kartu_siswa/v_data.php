<div class="col-md-12">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">DATA KARTU PESERTA UJIAN MTs NURUT TAQWA</h3>

              <div class="box-tools pull-right">
               
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">              
               <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>JENIS</th>
                    <th>TAHUN AJARN</th>
                    <th>KELAS</th>
                    <th>STATUS</th>
                    <th>RUANGAN</th>
                    <th>PILIHAN</th>
                    
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1; foreach ($r_data as $row_data): ?>
                  <?php
                    $H =   '<button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>';
                    $A = '<button class="btn btn-danger"><i class="glyphicon glyphicon-off"></i></button>';
                  ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_NO_INDUK ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_NAMA ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_JENIS ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_TAHUN_AJARAN ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_KELAS." - ".$row_data->KARTU_SISWA_PARALEL ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_NO ?></td>
                      <td><?php echo $row_data->KARTU_SISWA_RUANGAN ?></td>
                      <td>
                        <a href="" class="btn btn-success"><i class="fa fa-print"></i></a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
               
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
    })
  })
</script>
