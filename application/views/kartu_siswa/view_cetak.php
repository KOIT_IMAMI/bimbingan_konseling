<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <!-- Bootstrap 3.3.7 -->
</head>
<body>
  <?php $no=1; foreach ($r_data as $row): ?>
  <div style="width: 350px;height: 225px;float: left;padding-left: 5px;margin-bottom: 5px;">
    <table cellpadding="0" cellspacing="0"  style="font-family: times;font-size: 12px;">
    <tr >
      <td align="center" style="border-bottom:1px solid black;border-left:1px solid black;border-top:1px solid black">
        <img width="60" src="<?php echo base_url() ?>assets/mtslogohitam.png">
      </td>
      <td style="border-bottom:1px solid black;border-right:1px solid black;border-top:1px solid black;font-size: 14px;"><b>KARTU PESERTA UJIAN</b><br>PANITIA PENYELENGGARA PAS GANJIL<br>
        MTsS NURUT TAQWA<br>
        Tahun Pelajaran 2018-2019
      </td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;padding-left: 3px;">No. Peserta</td>
      <td style="border-right: 1px solid black;">: <?php echo $row->KARTU_SISWA_NO ?></td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;padding-left: 3px;">Nama Siswa</td>
      <td style="border-right: 1px solid black;">: <?php echo $row->KARTU_SISWA_NAMA ?></td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;padding-left: 3px;">No Induk</td>
      <td style="border-right: 1px solid black;">: <?php echo $row->KARTU_SISWA_NO_INDUK ?></td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;padding-left: 3px;">Ruangan</td>
      <td style="border-right: 1px solid black;">: <?php echo $row->KARTU_SISWA_RUANGAN ?></td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;">
        
      </td style="padding-left: 10px;border-right: 1px solid black;">
      <td style="border-right: 1px solid black;">Bondowoso, 12 Desember 2018<br>Kepala Madrasah,</td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;padding-left: 2px;">
        <?php $qr = base_url().'assets/qr/kartu_siswa/'.$row->KARTU_SISWA_QR.'.png' ?>
        </td>
      <td style="border-right: 1px solid black;">
        <img width="55" src="../assets/ttd_shomad.png"><img style="position: relative;left: 120px;" width="50" src="<?php echo $qr ?>">
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
      <td style="border-bottom: 1px solid black;border-right: 1px solid black;"><span style="position: relative;top: -10px;"><u><b>ABD. SHOMAD, M.Pd.I</b></u></span></td>
    </tr>
    
  </table>
    
  </div>
  <?php if ($no%10==0): ?>
    <div style="page-break-after: always;"> </div>
  <?php endif ?>
  

  <?php $no++; endforeach ?>
</body>
</html>
<script type="text/javascript">
  window.print();
</script>