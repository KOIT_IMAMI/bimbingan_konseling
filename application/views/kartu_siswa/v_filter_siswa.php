<div class="form-group">
  <label for="inputPassword3" class="col-sm-4 control-label">NAMA SISWA (OPSIONAL)</label>
  <div class="col-sm-8">
   <select id="ID_SISWA" name="ID_SISWA" class="form-control select2">
     <option value="">Semua Siswa</option>
     <?php foreach ($r_data as $row): ?>
     	<option value="<?php echo $row->ID_SISWA ?>"><?php echo $row->NAMA_SISWA ?></option>
     <?php endforeach ?>
   </select>
  </div>
</div>