<?php 
class M_apps extends CI_Model{

	function tampil_data($table){
		 $this->db->from($table);
        $query = $this->db->get();
        return $query->result(); 
	}
 
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	function delete_data($data,$table){
		$this->db->where($data);
		$this->db->delete($table);
	}
	function edit_data($filter,$table){		
		 $this->db->from($table);
        $this->db->where($filter);
 
        $query = $this->db->get();
 
        if ($query->num_rows() == 1) {
            return $query->row();
        }
	}
	function update_data($filter,$data,$table){
		$this->db->where($filter);
		$this->db->update($table,$data);
	}	

	function check_data($filter, $table){
		$this->db->from($table);
		$this->db->where($filter);
		$query = $this->db->get();
        return $query->row();
         
	}

	function check_data_result($filter, $table){
		$this->db->from($table);
		$this->db->where($filter);
		$query = $this->db->get();
        return $query->result();  
	}

	function check_data_num_rows($filter, $table){
		$this->db->from($table);
		$this->db->where($filter);
		$query = $this->db->get();
        return $query->num_rows(); 
	}

}
 ?>