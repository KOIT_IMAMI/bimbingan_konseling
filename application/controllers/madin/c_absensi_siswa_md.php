<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_absensi_siswa_md extends CI_Controller {

	public function index()
	{
		$data['data_tahun_ajaran'] 	= $this->M_apps->tampil_data('tahun_ajaran');
		$data['konten'] 			= 'absensi_siswa/view_list';
		$this->load->view('tampilan_backend',$data);
	}

	public function progress($id='')
	{
		$data['status'] = $id;
		$this->load->view('absensi_siswa/Progress',$data);
	}

	public function form()
	{
		$data['data_tahun_ajaran'] 	= $this->M_apps->tampil_data('tahun_ajaran');
		$data['konten'] 			= 'absensi_siswa/view';
		$this->load->view('tampilan_backend',$data);
	}

	public function data()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		$TANGGAL_MULAI 	= $this->input->post('TANGGAL_MULAI');
		$TANGGAL_AKHIR 	= $this->input->post('TANGGAL_AKHIR');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR'";		# code...
		}else{
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ID_SISWA ='$ID_SISWA' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR'";
		}


		$data['KELAS_PARALEL'] 	= $this->input->post('KELAS_PARALEL');
		$data['PARALEL'] 		= $this->input->post('PARALEL');
		$data['TAHUN_AJARAN'] 	= $this->input->post('TAHUN_AJARAN');
		$data['SEMESTER'] 		= $this->input->post('SEMESTER');
		$data['TANGGAL_MULAI'] 	= $this->input->post('TANGGAL_MULAI');
		$data['TANGGAL_AKHIR'] 	= $this->input->post('TANGGAL_AKHIR');
		$data['ID_SISWA'] 		= $this->input->post('ID_SISWA');		
		
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('absensi_siswa/v_data',$data);
	}

	public function ambil_data_siswa()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		
		$filter = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL
		);

		$data['r_data'] = $this->M_apps->check_data_result($filter,'tb_siswa_lengkap_aktif');
		$this->load->view('absensi_siswa/v_filter_siswa',$data);
	}

	public function list_data()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');

		$filter = array(
			'TAHUN_AJARAN_ID'=>'2'
		);

		$data['r_data'] = $this->M_apps->check_data_result($filter,'v_absensi_siswa');
		$this->load->view('absensi_siswa/v_data',$data);
	}

	public function form_add()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TANGGAL 		= $this->input->post('TANGGAL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		
		$filter = array(
			'TAHUN_AJARAN_ID'=>$TAHUN_AJARAN,
			'SISWA_KELAS'=>$KELAS_PARALEL,
			'ABSENSI_SISWA_PARALEL'=>$PARALEL,
			'ABSENSI_SISWA_TGL'=>$TANGGAL,
			'ABSENSI_SISWA_SEMESTER'=>$SEMESTER
		);

		$filter_siswa = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL,
			'STATUS'=>'AKTIF'
		);

		$filter_thn = array('TAHUN_AJARAN_ID'=>$TAHUN_AJARAN);

		$cek = $this->M_apps->check_data_num_rows($filter,'v_absensi_siswa');

		if ($cek>0) {
			$data['status_form'] 	= $cek;
			$data['ket_form'] 		= 'UPDATE DATA REKAP ABSENSI SISWA';
			$data['data_siswa'] 	= $this->M_apps->check_data_result($filter,'v_absensi_siswa');
		}else{
			$data['status_form'] 	= 0;
			$data['ket_form'] 		= 'INPUT DATA REKAP ABSENSI SISWA';
			$data['data_siswa'] 	= $this->M_apps->check_data_result($filter_siswa,'v_siswa_lengkap_aktif_all');
		}
		$data['TANGGAL'] 			= $this->M_public_function->get_date($TANGGAL);
		$data['ABSENSI_SISWA_TGL'] 	= $TANGGAL;
		$data['TAHUN_AJARAN'] 		= $this->M_apps->edit_data($filter_thn,'tahun_ajaran');
		$data['KELAS_PARALEL'] 		= $KELAS_PARALEL;
		$data['PARALEL'] 			= $PARALEL;
		$data['SEMESTER'] 			= $SEMESTER;
		$data['TAHUN_AJARAN_ID'] 	= $TAHUN_AJARAN;
		$this->load->view('absensi_siswa/form_add',$data);
	}

	public function simpan_data()
	{
		$KELAS_PARALEL 	= $this->input->post('SISWA_KELAS');
		$PARALEL 		= $this->input->post('ABSENSI_SISWA_PARALEL');
		$STATUS_FORM 	= $this->input->post('STATUS_FORM');

		$filter = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL
		);

		$filter_absensi = array(
			'SISWA_KELAS'=>$KELAS_PARALEL,
			'ABSENSI_SISWA_PARALEL'=>$PARALEL
		);
		
		if ($STATUS_FORM>0) {
			$db_absensi = $this->M_apps->check_data_result($filter_absensi,'v_absensi_siswa');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['ABSENSI_SISWA_ID'] 			= $this->input->post('ABSENSI_SISWA_ID'.$id);
				$data['ABSENSI_SISWA_TGL'] 			= $this->input->post('ABSENSI_SISWA_TGL');
				$data['ABSENSI_SISWA_NAMA'] 		= $this->input->post('ABSENSI_SISWA_NAMA'.$id);
				$data['ABSENSI_SISWA_KETERANGAN'] 	= $this->input->post('ABSENSI_SISWA_KETERANGAN');
				$data['ID_SISWA'] 					= $this->input->post('ID_SISWA'.$id);
				$data['SISWA_KELAS'] 				= $this->input->post('SISWA_KELAS');
				$data['SISWA_NO_INDUK'] 			= $this->input->post('SISWA_NO_INDUK'.$id);
				$data['ABSENSI_SISWA_TAHUN_AJARAN'] = $this->input->post('ABSENSI_SISWA_TAHUN_AJARAN');
				$data['ABSENSI_SISWA_SEMESTER'] 	= $this->input->post('ABSENSI_SISWA_SEMESTER');
				$data['TAHUN_AJARAN_ID'] 			= $this->input->post('TAHUN_AJARAN_ID');
				$data['ABSENSI_SISWA_PARALEL'] 		= $this->input->post('ABSENSI_SISWA_PARALEL');
				$data['ABSENSI_SISWA_JAM_I'] 		= $this->input->post('ABSENSI_SISWA_JAM_I'.$id);
				$data['ABSENSI_SISWA_JAM_II'] 		= $this->input->post('ABSENSI_SISWA_JAM_II'.$id);
				$data['ABSENSI_SISWA_JAM_III'] 		= $this->input->post('ABSENSI_SISWA_JAM_III'.$id);
				$data['ABSENSI_SISWA_JAM_IV'] 		= $this->input->post('ABSENSI_SISWA_JAM_IV'.$id);

				$filter = array(
					'ABSENSI_SISWA_ID'=>$data['ABSENSI_SISWA_ID'],
				);
				$this->M_apps->update_data($filter,$data,'absensi_siswa');
			}
			
		}else{
			$db_absensi = $this->M_apps->check_data_result($filter,'tb_siswa_lengkap_aktif');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['ABSENSI_SISWA_ID'] = '';
				$data['ABSENSI_SISWA_TGL'] = $this->input->post('ABSENSI_SISWA_TGL');
				$data['ABSENSI_SISWA_NAMA'] = $this->input->post('ABSENSI_SISWA_NAMA'.$id);
				$data['ABSENSI_SISWA_KETERANGAN'] = $this->input->post('ABSENSI_SISWA_KETERANGAN');
				$data['ID_SISWA'] = $this->input->post('ID_SISWA'.$id);
				$data['SISWA_KELAS'] = $this->input->post('SISWA_KELAS');
				$data['SISWA_NO_INDUK'] = $this->input->post('SISWA_NO_INDUK'.$id);
				$data['ABSENSI_SISWA_TAHUN_AJARAN'] = $this->input->post('ABSENSI_SISWA_TAHUN_AJARAN');
				$data['ABSENSI_SISWA_SEMESTER'] = $this->input->post('ABSENSI_SISWA_SEMESTER');
				$data['TAHUN_AJARAN_ID'] = $this->input->post('TAHUN_AJARAN_ID');
				$data['ABSENSI_SISWA_PARALEL'] = $this->input->post('ABSENSI_SISWA_PARALEL');
				$data['ABSENSI_SISWA_JAM_I'] = $this->input->post('ABSENSI_SISWA_JAM_I'.$id);
				$data['ABSENSI_SISWA_JAM_II'] = $this->input->post('ABSENSI_SISWA_JAM_II'.$id);
				$data['ABSENSI_SISWA_JAM_III'] = $this->input->post('ABSENSI_SISWA_JAM_III'.$id);
				$data['ABSENSI_SISWA_JAM_IV'] = $this->input->post('ABSENSI_SISWA_JAM_IV'.$id);

				$this->M_apps->input_data($data,'absensi_siswa');
			}
		}
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function hapus_data()
	{

		$KELAS_PARALEL = $this->input->post('SISWA_KELAS');
		$PARALEL = $this->input->post('ABSENSI_SISWA_PARALEL');
		$STATUS_FORM = $this->input->post('STATUS_FORM');

		$filter_absensi = array(
			'SISWA_KELAS'=>$KELAS_PARALEL,
			'ABSENSI_SISWA_PARALEL'=>$PARALEL
		);

		$db_absensi = $this->M_apps->check_data_result($filter_absensi,'v_absensi_siswa');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['ABSENSI_SISWA_ID'] = $this->input->post('ABSENSI_SISWA_ID'.$id);
				
				$filter = array(
					'ABSENSI_SISWA_ID'=>$data['ABSENSI_SISWA_ID'],
				);
				$this->M_apps->delete_data($filter,'absensi_siswa');
			}
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function cetak()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		$TANGGAL_MULAI 	= $this->input->post('TANGGAL_MULAI');
		$TANGGAL_AKHIR 	= $this->input->post('TANGGAL_AKHIR');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' ORDER BY ABSENSI_SISWA_TGL ASC ";		# code...
		}else{
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ID_SISWA ='$ID_SISWA' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' ORDER BY ABSENSI_SISWA_TGL ASC";
		}	
		
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('absensi_siswa/view_cetak', $data, FALSE);
	}

	public function cetak_rekap_data()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		$TANGGAL_MULAI 	= $this->input->post('TANGGAL_MULAI');
		$TANGGAL_AKHIR 	= $this->input->post('TANGGAL_AKHIR');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT *, SUM(IF(ABSENSI_SISWA_JAM_I='A',1,0)) AS JAM1, SUM(IF(ABSENSI_SISWA_JAM_II='A',1,0)) AS JAM2,SUM(IF(ABSENSI_SISWA_JAM_III='A',1,0)) AS JAM3,SUM(IF(ABSENSI_SISWA_JAM_IV='A',1,0)) AS JAM4 FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' GROUP BY ID_SISWA";		# code...
		}else{
			$filter = "SELECT *, SUM(IF(ABSENSI_SISWA_JAM_I='A',1,0)) AS JAM1, SUM(IF(ABSENSI_SISWA_JAM_II='A',1,0)) AS JAM2,SUM(IF(ABSENSI_SISWA_JAM_III='A',1,0)) AS JAM3,SUM(IF(ABSENSI_SISWA_JAM_IV='A',1,0)) AS JAM4 FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ID_SISWA ='$ID_SISWA' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' GROUP BY ID_SISWA";
		}	
		$data['TANGGAL_MULAI'] 	= $this->input->post('TANGGAL_MULAI');
		$data['TANGGAL_AKHIR'] 	= $this->input->post('TANGGAL_AKHIR');
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('absensi_siswa/view_rekap_cetak', $data, FALSE);
	}
}
