<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_rombel_md extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customers_model','customers');
	}

	public function index()
	{
		$data['data_siswa_md'] 	= $this->db2->get('santri')->result();
		$data['data_tahun_ajaran'] 	= $this->M_apps->tampil_data('tahun_ajaran');
		$data['konten'] 			= 'absensi_siswa/view_list';
		$this->load->view('tampilan_backend',$data);
	}

}
