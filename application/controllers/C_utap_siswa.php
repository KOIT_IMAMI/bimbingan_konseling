<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_utap_siswa extends CI_Controller {

	public function index()
	{
		$data['data_tahun_ajaran'] 	= $this->M_apps->tampil_data('tahun_ajaran');
		$data['konten'] 			= 'utap_siswa/view_list';
		$this->load->view('tampilan_backend',$data);
	}

	public function progress($id='')
	{
		$data['status'] = $id;
		$this->load->view('utap_siswa/Progress',$data);
	}

	public function form()
	{
		$data['data_tahun_ajaran'] 	= $this->M_apps->tampil_data('tahun_ajaran');
		$data['konten'] 			= 'utap_siswa/view';
		$this->load->view('tampilan_backend',$data);
	}

	public function data()
	{
		$UTAP_PESANTREN_KELAS 	= $this->input->post('UTAP_PESANTREN_KELAS');
		$UTAP_PESANTREN_PARALEL 		= $this->input->post('UTAP_PESANTREN_PARALEL');
		$UTAP_PESANTREN_TAHUN_AJARAN 	= $this->input->post('UTAP_PESANTREN_TAHUN_AJARAN');
		$UTAP_PESANTREN_JENIS 		= $this->input->post('UTAP_PESANTREN_JENIS');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT * FROM `v_utap_pesantren` WHERE UTAP_PESANTREN_KELAS = '$UTAP_PESANTREN_KELAS' AND TAHUN_AJARAN_ID = '$UTAP_PESANTREN_TAHUN_AJARAN' AND UTAP_PESANTREN_PARALEL ='$UTAP_PESANTREN_PARALEL' AND UTAP_PESANTREN_JENIS = '$UTAP_PESANTREN_JENIS'";		# code...
		}else{
			$filter = "SELECT * FROM `v_utap_pesantren` WHERE UTAP_PESANTREN_KELAS = '$UTAP_PESANTREN_KELAS' AND TAHUN_AJARAN_ID = '$UTAP_PESANTREN_TAHUN_AJARAN' AND UTAP_PESANTREN_PARALEL ='$UTAP_PESANTREN_PARALEL' AND UTAP_PESANTREN_JENIS = '$UTAP_PESANTREN_JENIS' AND ID_SISWA = '$ID_SISWA'";
		}


		$data['KELAS_PARALEL'] 	= $this->input->post('UTAP_PESANTREN_KELAS');
		$data['PARALEL'] 		= $this->input->post('UTAP_PESANTREN_PARALEL');
		$data['TAHUN_AJARAN'] 	= $this->input->post('UTAP_PESANTREN_TAHUN_AJARAN');
		$data['SEMESTER'] 		= $this->input->post('UTAP_PESANTREN_JENIS');
		$data['ID_SISWA'] 		= $this->input->post('ID_SISWA');		
		
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('utap_siswa/v_data',$data);
	}

	public function ambil_data_siswa()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		
		$filter = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL
		);

		$data['r_data'] = $this->M_apps->check_data_result($filter,'tb_siswa_lengkap_aktif');
		$this->load->view('utap_siswa/v_filter_siswa',$data);
	}

	public function list_data()
	{
		$UTAP_PESANTREN_KELAS 	= $this->input->post('UTAP_PESANTREN_KELAS');
		$UTAP_PESANTREN_PARALEL 		= $this->input->post('UTAP_PESANTREN_PARALEL');
		$UTAP_PESANTREN_TAHUN_AJARAN 	= $this->input->post('UTAP_PESANTREN_TAHUN_AJARAN');
		$UTAP_PESANTREN_JENIS 		= $this->input->post('UTAP_PESANTREN_JENIS');

		$filter = array(
			'TAHUN_AJARAN_ID'=>'2'
		);

		$data['r_data'] = $this->M_apps->check_data_result($filter,'v_utap_pesantren');
		$this->load->view('utap_siswa/v_data',$data);
	}

	public function form_add()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$UTAP_PESANTREN_JENIS 		= $this->input->post('UTAP_PESANTREN_JENIS');
		
		$filter = array(
			'TAHUN_AJARAN_ID'=>$TAHUN_AJARAN,
			'UTAP_PESANTREN_KELAS'=>$KELAS_PARALEL,
			'UTAP_PESANTREN_PARALEL'=>$PARALEL,
			'UTAP_PESANTREN_JENIS'=>$UTAP_PESANTREN_JENIS
		);

		$filter_siswa = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL,
			'STATUS'=>'AKTIF'
		);

		$filter_thn = array('TAHUN_AJARAN_ID'=>$TAHUN_AJARAN);

		$cek = $this->M_apps->check_data_num_rows($filter,'v_utap_pesantren');

		if ($cek>0) {
			$data['status_form'] 	= $cek;
			$data['ket_form'] 		= 'UPDATE DATA UTAP SISWA';
			$data['data_siswa'] 	= $this->M_apps->check_data_result($filter,'v_utap_pesantren');
		}else{
			$data['status_form'] 	= 0;
			$data['ket_form'] 		= 'INPUT DATA UTAP SISWA';
			$data['data_siswa'] 	= $this->M_apps->check_data_result($filter_siswa,'v_siswa_lengkap_aktif_all');
		}
		
		$data['TAHUN_AJARAN'] 		= $this->M_apps->edit_data($filter_thn,'tahun_ajaran');
		$data['KELAS_PARALEL'] 		= $KELAS_PARALEL;
		$data['PARALEL'] 			= $PARALEL;
		$data['UTAP_PESANTREN_JENIS'] 			= $UTAP_PESANTREN_JENIS;
		$data['TAHUN_AJARAN_ID'] 	= $TAHUN_AJARAN;
		$this->load->view('utap_siswa/form_add',$data);
	}

	public function simpan_data()
	{
		$KELAS_PARALEL 	= $this->input->post('UTAP_PESANTREN_KELAS');
		$PARALEL 		= $this->input->post('UTAP_PESANTREN_PARALEL');
		$STATUS_FORM 	= $this->input->post('STATUS_FORM');

		$filter = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL,
			'STATUS'=>'AKTIF'
		);

		$filter_absensi = array(
			'UTAP_PESANTREN_KELAS'=>$KELAS_PARALEL,
			'UTAP_PESANTREN_PARALEL'=>$PARALEL
		);
		
		if ($STATUS_FORM>0) {
			$db_absensi = $this->M_apps->check_data_result($filter_absensi,'v_utap_pesantren');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['UTAP_PESANTREN_ID'] 			= $this->input->post('UTAP_PESANTREN_ID'.$id);
				$data['UTAP_PESANTREN_NAMA'] 		= $this->input->post('UTAP_PESANTREN_NAMA'.$id);
				$data['UTAP_PESANTREN_KETERANGAN'] 	= $this->input->post('UTAP_PESANTREN_KETERANGAN');
				$data['ID_SISWA'] 					= $this->input->post('ID_SISWA'.$id);
				$data['UTAP_PESANTREN_KELAS'] 		= $this->input->post('UTAP_PESANTREN_KELAS');
				$data['UTAP_PESANTREN_NO_INDUK'] 			= $this->input->post('UTAP_PESANTREN_NO_INDUK'.$id);
				$data['UTAP_PESANTREN_TAHUN_AJARAN'] = $this->input->post('UTAP_PESANTREN_TAHUN_AJARAN');
				$data['UTAP_PESANTREN_JENIS'] 	= $this->input->post('UTAP_PESANTREN_JENIS');
				$data['TAHUN_AJARAN_ID'] 			= $this->input->post('TAHUN_AJARAN_ID');
				$data['UTAP_PESANTREN_PARALEL'] 		= $this->input->post('UTAP_PESANTREN_PARALEL');
				$data['UTAP_PESANTREN_STATUS'] 		= $this->input->post('UTAP_PESANTREN_STATUS'.$id);

				$filter = array(
					'UTAP_PESANTREN_ID'=>$data['UTAP_PESANTREN_ID'],
				);
				$this->M_apps->update_data($filter,$data,'utap_pesantren');
			}
			
		}else{
			$db_absensi = $this->M_apps->check_data_result($filter,'tb_siswa_lengkap_aktif');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['UTAP_PESANTREN_ID'] 			= '';
				$data['UTAP_PESANTREN_NAMA'] 		= $this->input->post('UTAP_PESANTREN_NAMA'.$id);
				$data['UTAP_PESANTREN_KETERANGAN'] 	= $this->input->post('UTAP_PESANTREN_KETERANGAN');
				$data['ID_SISWA'] 					= $this->input->post('ID_SISWA'.$id);
				$data['UTAP_PESANTREN_KELAS'] 		= $this->input->post('UTAP_PESANTREN_KELAS');
				$data['UTAP_PESANTREN_NO_INDUK'] 	= $this->input->post('UTAP_PESANTREN_NO_INDUK'.$id);
				$data['UTAP_PESANTREN_TAHUN_AJARAN'] = $this->input->post('UTAP_PESANTREN_TAHUN_AJARAN');
				$data['UTAP_PESANTREN_JENIS'] 		= $this->input->post('UTAP_PESANTREN_JENIS');
				$data['TAHUN_AJARAN_ID'] 			= $this->input->post('TAHUN_AJARAN_ID');
				$data['UTAP_PESANTREN_PARALEL'] 	= $this->input->post('UTAP_PESANTREN_PARALEL');
				$data['UTAP_PESANTREN_STATUS'] 		= $this->input->post('UTAP_PESANTREN_STATUS'.$id);

				$this->M_apps->input_data($data,'utap_pesantren');
			}
		}
		echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function hapus_data()
	{

		$KELAS_PARALEL = $this->input->post('SISWA_KELAS');
		$PARALEL = $this->input->post('ABSENSI_SISWA_PARALEL');
		$STATUS_FORM = $this->input->post('STATUS_FORM');

		$filter_absensi = array(
			'SISWA_KELAS'=>$KELAS_PARALEL,
			'ABSENSI_SISWA_PARALEL'=>$PARALEL
		);

		$db_absensi = $this->M_apps->check_data_result($filter_absensi,'v_absensi_siswa');
			foreach ($db_absensi as $row_data) {
				$id = $row_data->ID_SISWA;
				$data['ABSENSI_SISWA_ID'] = $this->input->post('ABSENSI_SISWA_ID'.$id);
				
				$filter = array(
					'ABSENSI_SISWA_ID'=>$data['ABSENSI_SISWA_ID'],
				);
				$this->M_apps->delete_data($filter,'absensi_siswa');
			}
			echo json_encode(array("status"=>true,'pesan'=>'simpan'));
	}

	public function cetak()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		$TANGGAL_MULAI 	= $this->input->post('TANGGAL_MULAI');
		$TANGGAL_AKHIR 	= $this->input->post('TANGGAL_AKHIR');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' ORDER BY ABSENSI_SISWA_TGL ASC ";		# code...
		}else{
			$filter = "SELECT * FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ID_SISWA ='$ID_SISWA' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' ORDER BY ABSENSI_SISWA_TGL ASC";
		}	
		
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('utap_siswa/view_cetak', $data, FALSE);
	}

	public function cetak_rekap_data()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$TAHUN_AJARAN 	= $this->input->post('TAHUN_AJARAN');
		$SEMESTER 		= $this->input->post('SEMESTER');
		$TANGGAL_MULAI 	= $this->input->post('TANGGAL_MULAI');
		$TANGGAL_AKHIR 	= $this->input->post('TANGGAL_AKHIR');
		$ID_SISWA 		= $this->input->post('ID_SISWA');

		if (empty($ID_SISWA)) {
			$filter = "SELECT *, SUM(IF(ABSENSI_SISWA_JAM_I='A',1,0)) AS JAM1, SUM(IF(ABSENSI_SISWA_JAM_II='A',1,0)) AS JAM2,SUM(IF(ABSENSI_SISWA_JAM_III='A',1,0)) AS JAM3,SUM(IF(ABSENSI_SISWA_JAM_IV='A',1,0)) AS JAM4 FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' GROUP BY ID_SISWA";		# code...
		}else{
			$filter = "SELECT *, SUM(IF(ABSENSI_SISWA_JAM_I='A',1,0)) AS JAM1, SUM(IF(ABSENSI_SISWA_JAM_II='A',1,0)) AS JAM2,SUM(IF(ABSENSI_SISWA_JAM_III='A',1,0)) AS JAM3,SUM(IF(ABSENSI_SISWA_JAM_IV='A',1,0)) AS JAM4 FROM `v_absensi_siswa` WHERE SISWA_KELAS = '$KELAS_PARALEL' AND TAHUN_AJARAN_ID = '$TAHUN_AJARAN' AND ABSENSI_SISWA_PARALEL ='$PARALEL' AND ABSENSI_SISWA_SEMESTER = '$SEMESTER' AND ID_SISWA ='$ID_SISWA' AND ABSENSI_SISWA_TGL BETWEEN '$TANGGAL_MULAI' AND '$TANGGAL_AKHIR' GROUP BY ID_SISWA";
		}	
		$data['TANGGAL_MULAI'] 	= $this->input->post('TANGGAL_MULAI');
		$data['TANGGAL_AKHIR'] 	= $this->input->post('TANGGAL_AKHIR');
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('utap_siswa/view_rekap_cetak', $data, FALSE);
	}
}
